package com.greysonparrelli.dagger.input;

public interface IInput {
  public enum Key {
    LEFT,
    RIGHT,
    UP,
    DOWN
  }

  boolean isKeyDown(Key key);
}
