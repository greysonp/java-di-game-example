package com.greysonparrelli.dagger.di;

import javax.inject.*;

import dagger.*;

import com.greysonparrelli.dagger.*;

@Component(modules = GameModule.class)
@Singleton
public interface GameComponent {
  void inject(Main main);
}
