package com.greysonparrelli.dagger.di;

import java.awt.*;
import javax.inject.*;

import dagger.*;

import com.greysonparrelli.dagger.*;
import com.greysonparrelli.dagger.game.*;
import com.greysonparrelli.dagger.input.*;
import com.greysonparrelli.dagger.logging.*;
import com.greysonparrelli.dagger.renderer.*;

@Module(includes = GameModule.Bindings.class)
final public class GameModule {

  @Module
  public interface Bindings {
    @Singleton
    @Binds IInput bindInput(KeyboardInput input);

    @Singleton
    @Binds ILogger bindLogger(SystemLogger logger);

    @Singleton
    @Binds IRenderer bindRenderer(TextRenderer renderer);
  }

  private final Container mContainer;
  private final int mWidth;
  private final int mHeight;

  public GameModule(Container container, int width, int height) {
    mContainer = container;
    mWidth = width;
    mHeight = height;
  }

  @Provides
  @Singleton
  Container provideContainer() {
    return mContainer;
  }

  @Provides
  @Named("game_width")
  int provideGameWidth() {
    return mWidth;
  }

  @Provides
  @Named("game_height")
  int provideGameHeight() {
    return mHeight;
  }

  @Provides
  KeyboardFocusManager provideKeyboardFocusManager() {
    return KeyboardFocusManager.getCurrentKeyboardFocusManager();
  }
}
