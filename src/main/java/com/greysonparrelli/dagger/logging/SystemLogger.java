package com.greysonparrelli.dagger.logging;

import javax.inject.Inject;

public class SystemLogger implements ILogger {

  @Inject
  public SystemLogger() {

  }

  @Override
  public void log(String message) {
    System.out.println(message);
  }
}
