package com.greysonparrelli.dagger.logging;

public interface ILogger {
  void log(String message);
}
