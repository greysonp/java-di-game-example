package com.greysonparrelli.dagger.game;

import javax.inject.Inject;
import java.awt.geom.*;

import com.greysonparrelli.dagger.logging.*;
import com.greysonparrelli.dagger.renderer.*;

public class Block {

  private static final float MAX_VELOCITY = 1.5f;
  private static final float MIN_VELOCITY = 0.25f;

  private final ILogger mLogger;
  private final IRenderer mRenderer;

  private float mX;
  private float mY;
  private float mVx;
  private float mVy;
  private boolean[] mSprite;

  @Inject
  public Block(ILogger logger, IRenderer renderer) {
    mLogger = logger;
    mRenderer = renderer;
    setSprite();
    setStartingPosition();
    setStartingVelocity();
  }

  public void update() {
    mX += mVx;
    mY += mVy;
  }

  public void draw() {
    int px = Math.round(mX);
    int py = Math.round(mY);
    for (int i = 0; i < mSprite.length; i++) {
      if (!mSprite[i]) {
        continue;
      }
      int x = px + i % 3;
      int y = py + i / 3;
      if (x < mRenderer.getWidth() - 1 &&
          x >= 0 &&
          y < mRenderer.getHeight() - 1 &&
          y >= 0) {
        mRenderer.setPixel(x, y, true);
      }
    }
  }

  private void setSprite() {
    mSprite = new boolean[9];
    for (int i = 0; i < mSprite.length; i++) {
      mSprite[i] = Math.random() > 0.4;
    }
    mSprite[4] = true;
  }

  private void setStartingPosition() {
    int halfWidth = mRenderer.getWidth() / 2;
    int halfHeight = mRenderer.getHeight() / 2;
    int radius = (int)(Math.sqrt(halfWidth * halfWidth + halfHeight * halfHeight)) + 3;
    float angle = (float)(Math.random() * Math.PI * 2);
    mX = (float)(Math.cos(angle) * radius) + halfWidth;
    mY = (float)(Math.sin(angle) * radius) + halfHeight;
  }

  private void setStartingVelocity() {
    float tX = (float) Math.round(Math.random() * mRenderer.getWidth());
    float tY = (float) Math.round(Math.random() * mRenderer.getHeight());

    float dX = tX - mX;
    float dY = tY - mY;

    float angle = (float) Math.atan2(dY, dX);
    float velocity = (float)(MIN_VELOCITY + (Math.random() * (MAX_VELOCITY - MIN_VELOCITY)));

    mVx = (float)(Math.cos(angle) * velocity);
    mVy = (float)(Math.sin(angle) * velocity);
  }
}
