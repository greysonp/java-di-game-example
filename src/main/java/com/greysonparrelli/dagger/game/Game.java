package com.greysonparrelli.dagger;

import java.awt.Point;
import java.util.*;
import javax.inject.*;

import com.greysonparrelli.dagger.game.*;
import com.greysonparrelli.dagger.input.*;
import com.greysonparrelli.dagger.logging.*;
import com.greysonparrelli.dagger.renderer.*;
import com.greysonparrelli.dagger.input.IInput.Key;

public class Game {

  private final ILogger mLogger;
  private final IRenderer mRenderer;
  private final IInput mInput;
  private final Provider<Block> mBlockProvider;

  private final Point mPlayer;
  private final List<Block> mBlocks;

  private int mBlockSpawnTime = 45;
  private int mBlockTime = mBlockSpawnTime;
  private boolean mGameOver = false;

  @Inject
  public Game(ILogger logger, IRenderer renderer, IInput input, Provider<Block> blockProvider) {
    mLogger = logger;
    mRenderer = renderer;
    mInput = input;
    mBlockProvider = blockProvider;

    mPlayer = new Point(mRenderer.getWidth()/2, mRenderer.getHeight()/2);
    mBlocks = new ArrayList<>();

    mBlocks.add(mBlockProvider.get());
  }

  public void update() {
    if (mGameOver) {
      return;
    }
    updateBlockState();
    updatePlayerState();
    draw();
  }

  private void updateBlockState() {
    for (Block block : mBlocks) {
      block.update();
    }

    if (mBlockTime == 0) {
      mBlockTime = mBlockSpawnTime = Math.max(mBlockSpawnTime - 1, 5);
      mBlocks.add(mBlockProvider.get());
    }
    mBlockTime--;
  }

  private void updatePlayerState() {
    if (mInput.isKeyDown(Key.LEFT)) {
      mPlayer.x = Math.max(mPlayer.x - 1, 0);
    }
    if (mInput.isKeyDown(Key.RIGHT)) {
      mPlayer.x = Math.min(mPlayer.x + 1, mRenderer.getWidth() - 1);
    }
    if (mInput.isKeyDown(Key.UP)) {
      mPlayer.y = Math.max(mPlayer.y - 1, 0);
    }
    if (mInput.isKeyDown(Key.DOWN)) {
      mPlayer.y = Math.min(mPlayer.y + 1, mRenderer.getHeight() - 1);
    }
  }

  private void draw() {
    mRenderer.clear();
    for (Block block : mBlocks) {
      block.draw();
    }
    if (mRenderer.getPixel(mPlayer.x, mPlayer.y)) {
      mGameOver = true;
    }
    mRenderer.setPixel(mPlayer.x, mPlayer.y, true);
    mRenderer.draw();
  }
}
