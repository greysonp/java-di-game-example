package com.greysonparrelli.dagger;

import java.awt.*;
import java.awt.event.*;
import javax.inject.Inject;
import javax.swing.*;

import com.greysonparrelli.dagger.di.*;
import com.greysonparrelli.dagger.game.*;
import com.greysonparrelli.dagger.input.*;
import com.greysonparrelli.dagger.logging.*;
import com.greysonparrelli.dagger.renderer.*;

public class Main {

  private static final int FPS = 30;

  @Inject
  Game mGame;

  public static void main(String[] args) {
    final Main main = new Main();
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
          main.init();
      }
    });
  }

  private void init() {
    //Create and set up the window.
    JFrame frame = new JFrame("My Little Game");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Initialize DI
    GameComponent gameComponent = DaggerGameComponent.builder()
        .gameModule(new GameModule(frame.getContentPane(), 64, 32))
        .build();
    gameComponent.inject(this);
    mGame.update();

    // Setup a loop to update our game every frame
    int delay = 1000 / FPS;
    Timer timer = new Timer(delay, new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        mGame.update();
      }
    });
    timer.setRepeats(true);
    timer.start();

    //Display the window.
    frame.pack();
    frame.setVisible(true);
  }
}
