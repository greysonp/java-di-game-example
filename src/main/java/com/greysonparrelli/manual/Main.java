package com.greysonparrelli.manual;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.greysonparrelli.manual.input.*;
import com.greysonparrelli.manual.logging.*;
import com.greysonparrelli.manual.renderer.*;

public class Main {

  private static final int FPS = 30;

  public static void main(String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
          createAndShowGUI();
      }
    });
  }

  private static void createAndShowGUI() {
    //Create and set up the window.
    JFrame frame = new JFrame("My Little Game");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Intialize our game and all of its components
    ILogger logger = new SystemLogger();
    IRenderer renderer = new TextRenderer(
        logger,
        frame.getContentPane(),
        new TextRenderer.JTextAreaFactory(),
        64,
        32);
    IInput input = new KeyboardInput(
        logger,
        KeyboardFocusManager.getCurrentKeyboardFocusManager());
    final Game game = new Game(logger, renderer, input);
    game.update();

    // Setup a loop to update our game every frame
    int delay = 1000 / FPS;
    Timer timer = new Timer(delay, new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        game.update();
      }
    });
    timer.setRepeats(true);
    timer.start();

    //Display the window.
    frame.pack();
    frame.setVisible(true);
  }
}
