package com.greysonparrelli.manual;

import java.awt.Point;

import com.greysonparrelli.manual.input.*;
import com.greysonparrelli.manual.logging.*;
import com.greysonparrelli.manual.renderer.*;
import com.greysonparrelli.manual.input.IInput.Key;

public class Game {

  private final ILogger mLogger;
  private final IRenderer mRenderer;
  private final IInput mInput;

  private final Point mPlayer;

  public Game(ILogger logger, IRenderer renderer, IInput input) {
    mLogger = logger;
    mRenderer = renderer;
    mInput = input;

    mPlayer = new Point(0, 0);
  }

  public void update() {
    updateState();
    draw();
  }

  private void updateState() {
    if (mInput.isKeyDown(Key.LEFT)) {
      mPlayer.x = Math.max(mPlayer.x - 1, 0);
    }
    if (mInput.isKeyDown(Key.RIGHT)) {
      mPlayer.x = Math.min(mPlayer.x + 1, mRenderer.getWidth() - 1);
    }
    if (mInput.isKeyDown(Key.UP)) {
      mPlayer.y = Math.max(mPlayer.y - 1, 0);
    }
    if (mInput.isKeyDown(Key.DOWN)) {
      mPlayer.y = Math.min(mPlayer.y + 1, mRenderer.getHeight() - 1);
    }
  }

  private void draw() {
    mRenderer.clear();
    mRenderer.setPixel(mPlayer.x, mPlayer.y, true);
    mRenderer.draw();
  }
}
