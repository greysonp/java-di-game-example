package com.greysonparrelli.manual.input;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import com.greysonparrelli.manual.input.IInput.Key;
import com.greysonparrelli.manual.logging.*;

public class KeyboardInput implements IInput, KeyEventDispatcher {

  private static final Map<Integer, IInput.Key> KEY_MAPPING = new HashMap<>();
  static {
    KEY_MAPPING.put(37, Key.LEFT);
    KEY_MAPPING.put(38, Key.UP);
    KEY_MAPPING.put(39, Key.RIGHT);
    KEY_MAPPING.put(40, Key.DOWN);
  }

  private final ILogger mLogger;
  private final Map<IInput.Key, Boolean> mKeys;

  public KeyboardInput(
      ILogger logger,
      KeyboardFocusManager keyboardFocusManager) {
    mLogger = logger;

    keyboardFocusManager.addKeyEventDispatcher(this);
    mKeys = new HashMap<>();
  }

  @Override
  public boolean isKeyDown(Key key) {
    return mKeys.containsKey(key) && mKeys.get(key);
  }

  @Override
  public boolean dispatchKeyEvent(KeyEvent keyEvent) {
    if (KEY_MAPPING.containsKey(keyEvent.getKeyCode())) {
      mKeys.put(
          KEY_MAPPING.get(keyEvent.getKeyCode()),
          keyEvent.getID() == KeyEvent.KEY_PRESSED);
    }
    return false;
  }
}
