package com.greysonparrelli.manual.renderer;

import java.awt.*;
import javax.swing.*;

import com.greysonparrelli.manual.logging.*;

public class TextRenderer implements IRenderer {

  private static final String TEXT_NAME = "text_renderer_text_field";

  private final int WIDTH;
  private final int HEIGHT;

  private final ILogger mLogger;
  private final Container mContainer;
  private final JTextAreaFactory mJTextAreaFactory;
  private final JTextArea mTextArea;
  private final boolean[] mVideoBuffer;
  private final StringBuilder mTextOutput = new StringBuilder();

  public TextRenderer(
      ILogger logger,
      Container container,
      JTextAreaFactory jTextAreaFactory,
      int width,
      int height) {
    mLogger = logger;
    mContainer = container;
    mJTextAreaFactory = jTextAreaFactory;
    WIDTH = width;
    HEIGHT = height;

    mVideoBuffer = new boolean[WIDTH * HEIGHT];
    mTextArea = addTextArea();
  }

  @Override
  public boolean getPixel(int x, int y) {
    return mVideoBuffer[getVideoBufferIndex(x, y)];
  }

  @Override
  public void setPixel(int x, int y, boolean state) {
    mVideoBuffer[getVideoBufferIndex(x, y)] = state;
  }

  @Override
  public void draw() {
    mTextOutput.setLength(0);
    for (int i = 0; i < mVideoBuffer.length; i++) {
      if (i > 0 && i % WIDTH == 0) {
        mTextOutput.append('\n');
      }
      mTextOutput.append(mVideoBuffer[i] ? 'X' : ' ');
    };
    mTextArea.setText(mTextOutput.toString());
  }

  @Override
  public void clear() {
    for (int i = 0; i < mVideoBuffer.length; i++) {
      mVideoBuffer[i] = false;
    }
    draw();
  }

  @Override
  public int getWidth() {
    return WIDTH;
  }

  @Override
  public int getHeight() {
    return HEIGHT;
  }

  private int getVideoBufferIndex(int x, int y) {
    return y * WIDTH + x;
  }

  private JTextArea addTextArea() {
    JTextArea textArea = mJTextAreaFactory.create();
    textArea.setName(TEXT_NAME);
    textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
    mContainer.add(textArea);
    return textArea;
  }

  public static class JTextAreaFactory {
    public JTextArea create() {
      return new JTextArea();
    }
  }
}
