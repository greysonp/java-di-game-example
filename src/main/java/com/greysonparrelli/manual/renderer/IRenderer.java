package com.greysonparrelli.manual.renderer;

import java.awt.*;

public interface IRenderer {
  void setPixel(int x, int y, boolean state);
  boolean getPixel(int x, int y);
  int getWidth();
  int getHeight();
  void draw();
  void clear();
}
