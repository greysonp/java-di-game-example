package com.greysonparrelli.manual.logging;

public interface ILogger {
  void log(String message);
}
