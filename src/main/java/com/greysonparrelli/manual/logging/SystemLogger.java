package com.greysonparrelli.manual.logging;

public class SystemLogger implements ILogger {

  @Override
  public void log(String message) {
    System.out.println(message);
  }
}
